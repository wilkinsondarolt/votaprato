class Prato < ApplicationRecord
  validates_presence_of(:nome, message: " - deve ser preenchido")
  validates_uniqueness_of(:nome, message: " - nome já cadastrado")
  validate :validates_presence_of_more_than_one_restaurante
  has_and_belongs_to_many :restaurantes
  has_one :receita

  private
  def validates_presence_of_more_than_one_restaurante
    erros.add("restaurantes",
              "deve haves ao menos um restaurante") if restaurante.empty?
  end
end
