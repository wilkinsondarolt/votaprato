class Restaurante < ApplicationRecord
  validates_presence_of(:nome, message: "deve ser preenchido")
  validates_presence_of(:endereco, message: "deve ser preenchido")
  validates_presence_of(:especialidade, message: "deve ser preenchdio")

  validates_uniqueness_of(:nome, message: "nome ja cadastrado.")
  validates_uniqueness_of(:endereco, message: "endereço já cadastrado")

  validate :primeira_letra_maiuscula

  private
  def primeira_letra_maiuscula
    errors.add(:nome, "Primeira letra deve ser maiúscula") unless nome =~ /[A-Z].*/
  end
  has_many :qualificacoes
  has_and_belongs_to_many :pratos
end
